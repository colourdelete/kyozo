from modules import *
import base
from panda3d.bullet import *
from direct.task import Task


class bullet(base.base):
  def __init__(self, gravity=(0, 0, -9.807), bullet_debug_render=True, **kwargs):
    base.base.__init__(self, **kwargs)
    if self.var_check: checker.var_check('gravity', [list(), tuple()], gravity)
    if self.var_check: checker.var_check('bullet_debug_render', bool(), bullet_debug_render)
    self.gravity = gravity
    self.world = BulletWorld()
    self.world.setGravity(Vec3(*gravity))
    self.add_task(self.bullet_update, 'bullet_update')
    self.bullet_debug_render = bullet_debug_render
    if self.bullet_debug_render:
      self.debugNode = BulletDebugNode('Debug')
      self.debugNode.showWireframe(True)
      self.debugNode.showConstraints(True)
      self.debugNode.showBoundingBoxes(True)
      self.debugNode.showNormals(True)
      self.debugNP = self.render.attachNewNode(self.debugNode)
      self.debugNP.show()
      self.world.setDebugNode(self.debugNP.node())

  def bullet_update(self, task):
    dt = globalClock.getDt()
    self.world.doPhysics(dt)
    return task.cont

  def add_bullet_plane(self, pos=(0, 0, 2), size=(0.5, 0.5, 0.5), nvec=(0, 0, 1), d=1):
    if self.var_check: checker.var_check('pos', [list(), tuple()], pos)
    if self.var_check: checker.var_check('size', [list(), tuple()], size)
    if self.var_check: checker.var_check('nvec', [list(), tuple()], nvec)
    if self.var_check: checker.var_check('d', [int(), float()], d)
    shape = BulletPlaneShape(Vec3(*nvec), d)
    node = BulletRigidBodyNode('Ground')
    node.addShape(shape)
    np = self.render.attachNewNode(node)
    np.setPos(*pos)
    self.world.attachRigidBody(node)
    # shape = BulletBoxShape(Vec3(*size))
    #
    # node = BulletRigidBodyNode('Box')
    # node.setMass(mass)
    # node.addShape(shape)
    #
    # np = self.render.attachNewNode(node)
    # np.setPos(*pos)
    # self.world.attachRigidBody(node)

  def add_bullet_box(self, pos=(0, 0, 2), size=(0.5, 0.5, 0.5), mass=1.0, skin=None):#'models/box.egg'):
    if self.var_check: checker.var_check('pos', [list(), tuple()], pos)
    if self.var_check: checker.var_check('size', [list(), tuple()], size)
    if self.var_check: checker.var_check('mass', [int(), float()], mass)
    if self.var_check: checker.var_check('skin', [str(), None], skin)
    shape = BulletBoxShape(Vec3(*size))
    node = BulletRigidBodyNode('Box')
    node.setMass(mass)
    node.addShape(shape)
    np = self.render.attachNewNode(node)
    np.setPos(*pos)
    self.world.attachRigidBody(node)
    if not not skin:
      model = loader.loadModel(skin)
      model.flattenLight()
      model.reparentTo(np)

if __name__ == '__main__':
  app = bullet()
  app.add_bullet_plane()
  for i in range(0, 50, 5):
    app.add_bullet_box(pos=(0, 0, i))
  app.run()