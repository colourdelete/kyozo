import base
import checker


class interactive(base.base):
  def __init__(self):
    verbose = True
    '''Class for interactive stuff like fpv.'''
    self.accept('w', self.task_cam_fpv_handler, ['w'])
    self.accept('a', self.task_cam_fpv_handler, ['a'])
    self.accept('s', self.task_cam_fpv_handler, ['s'])
    self.accept('d', self.task_cam_fpv_handler, ['d'])
    self.accept('i', self.task_cam_fpv_handler, ['i'])
    self.accept('k', self.task_cam_fpv_handler, ['k'])
    self.accept('q', self.task_cam_fpv_handler, ['q'])
    self.task_cam_fpv_xspeed = 0
    self.task_cam_fpv_yspeed = 0
    self.task_cam_fpv_zspeed = 0
    print('yeehaw')
    self.disableMouse()
    print('yeehaw2')
    self.add_task(self.task_cam_fpv, 'task_cam_fpv')
    print('yeehaw3')
    # if verbose: print(verbose_report.format('Done', 'OK', 'Set vars for interactive'))

  def task_cam_fpv_handler(self, key):
    print('{} {} {} {}'.format(key, self.task_cam_fpv_xspeed, self.task_cam_fpv_yspeed, self.task_cam_fpv_zspeed))
    if key == 'w':
      self.task_cam_fpv_xspeed = 1
    elif key == 's':
      self.task_cam_fpv_xspeed = -1
    elif key == 'a':
      self.task_cam_fpv_yspeed = 1
    elif key == 'd':
      self.task_cam_fpv_yspeed = -1
    elif key == 'i':
      self.task_cam_fpv_zspeed = 1
    elif key == 'k':
      self.task_cam_fpv_zspeed = -1
    elif key == 'q':
      self.task_cam_fpv_xspeed = 0
      self.task_cam_fpv_yspeed = 0
      self.task_cam_fpv_zspeed = 0

  def task_cam_fpv(self, task):
    md = self.win.getPointer(0)
    x = md.getX()
    x_max = self.win.getXSize()
    h = x/x_max*360
    y = md.getY()
    y_max = self.win.getYSize()
    p = y/y_max*180
    r = 0
    self.set_cam_hpr((h, p, r))
    delta_x = self.camera.getX() - self.task_cam_fpv_xspeed * task.time
    delta_y = self.camera.getY() - self.task_cam_fpv_yspeed * task.time
    delta_z = self.camera.getZ() - self.task_cam_fpv_zspeed * task.time
    print(delta_x)
    self.set_cam_pos((delta_x, delta_y, delta_z))
    self.set_cam_pos((0, -30, 8))
    return task.cont
